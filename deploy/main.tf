terraform {
  backend "s3" {
    bucket         = "shandr-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "eu-north-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-north-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Env       = terraform.workspace
    Project   = var.project
    Owner     = var.contact
    ManagedBy = "Terraform"
  }
}
